import { useEffect, useState, useCallback, useReducer } from "react";
import axios from "axios";

const INITIAL_STATE = {
  status: null,
  message: "",
};

function reducer(state, action) {
  let newState;
  switch (action.type) {
    case "success":
      newState = { status: "success", message: action.message };
      break;
    case "error":
      newState = { status: "severity", message: action.message };
      break;
    default:
      newState = { ...state };
  }

  return newState;
}

export default function useFetch({ method, url }, queryInput, setOpenSnackBar) {
  const [data, setData] = useState(null);
  const [dataStatus, dispatchDataStatus] = useReducer(reducer, INITIAL_STATE);

  const fetchQueryHandler = useCallback(async () => {
    if (!queryInput.trim().length) return;

    try {
      const res = await axios({
        method,
        url,
      });
      setData(res.data.similarWords);

      dispatchDataStatus({
        type: "success",
        message: `"${res.data.similarWords[0]}" is removed from corpus`,
      });

      setOpenSnackBar(true);
    } catch (error) {
      dispatchDataStatus({
        type: "error",
        message: `Something went wrong`,
      });
      setOpenSnackBar(true);
    }
  }, [method, url, queryInput, setOpenSnackBar]);

  useEffect(() => {
    const id = setTimeout(() => {
      fetchQueryHandler();
    }, 300);

    //cleanup function
    return () => clearTimeout(id);
  }, [fetchQueryHandler]);

  return { data, dataStatus };
}
