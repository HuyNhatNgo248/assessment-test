import {
  Container,
  Stack,
  useTheme,
  Box,
  Typography,
  styled,
} from "@mui/material";
import SearchBar from "../components/SearchBar";
import QuerySearch from "../context/QuerySearchContext";
import Logo from "../components/Logo";
import { useState } from "react";
import Medal from "../components/Medal";
import StatusSnackbar from "../components/StatusSnackbar";
import LoadingSpinner from "../components/LoadingSpinner";

const StyledContainer = styled(Container)(({ theme }) => ({
  position: "absolute",
  top: "20%",
  left: "50%",
  transform: "translateX(-50%)",
  padding: "0 !important",
  maxWidth: "100% !important",
  width: "max-content",

  [theme.breakpoints.down("sm")]: {
    width: "100%",
  },
}));

const StyledSearchContainer = styled(Box)(({ theme }) => ({
  borderRadius: theme.radius["md"],
  transition: "all 0.3s",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column",
  backgroundColor: theme.palette.color[0],
  "&:hover": {
    boxShadow: "0 0.5rem 0.5rem rgba(0,0,0,0.15)",
  },

  [theme.breakpoints.down("sm")]: {
    "&:hover": {
      boxShadow: "none",
    },
  },
}));

export default function SearchPage() {
  const { data, queryInput } = QuerySearch();

  const [barFocus, setBarFocus] = useState(false);

  const theme = useTheme();

  const barFocusHandler = (isFocused) => {
    return () => setBarFocus(isFocused);
  };

  let resultsBox;
  if (queryInput.length && barFocus) {
    resultsBox = (
      <Stack
        component="ul"
        sx={{
          width: "100%",
          padding: `0 ${theme.spacing(4)}`,
          gap: theme.spacing(2),
        }}
      >
        {!data ? (
          <LoadingSpinner />
        ) : (
          data.map((el, idx) => (
            <Box
              component="li"
              style={{
                display: "flex",
                listStyle: "none",
                gap: theme.spacing(1),
              }}
              key={el + idx}
            >
              <Medal
                type={idx}
                sx={{
                  fontSize: "1.5rem",
                }}
              />
              <Typography>{el}</Typography>
            </Box>
          ))
        )}
      </Stack>
    );
  }

  return (
    <>
      <StyledContainer>
        <Logo
          sx={{
            marginBottom: theme.spacing(4),
          }}
        />

        <StyledSearchContainer
          tabIndex={0}
          onFocus={barFocusHandler(true)}
          onBlur={barFocusHandler(false)}
          sx={{
            boxShadow: queryInput.length
              ? "0 0.5rem 0.5rem rgba(0,0,0,0.15)"
              : "none",
          }}
        >
          <SearchBar />
          {resultsBox}
        </StyledSearchContainer>
      </StyledContainer>
      <StatusSnackbar />
    </>
  );
}
