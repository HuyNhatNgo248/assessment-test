import { Typography, useTheme, Box, styled } from "@mui/material";
import { motion } from "framer-motion";
import { useNavigate } from "react-router-dom";
import StyledButton from "../custom/StyledButton";

const StyledTitle = styled(Typography)(({ theme }) => ({
  fontSize: "4rem",
  fontWeight: theme.typography.weight["bold"],
  color: theme.palette.color[700],

  [theme.breakpoints.down("lg")]: {
    fontSize: "3.6rem",
  },
  [theme.breakpoints.down("md")]: {
    fontSize: "3rem",
  },
  [theme.breakpoints.down("sm")]: {
    fontSize: "2.4rem",
  },
}));

const StyledSubtitle = styled(Typography)(({ theme }) => ({
  fontSize: "1.4rem",
  color: theme.palette.color[400],
  marginBottom: theme.spacing(4),

  [theme.breakpoints.down("lg")]: {
    fontSize: "1.3rem",
  },
  [theme.breakpoints.down("md")]: {
    fontSize: "1.2rem",
  },
  [theme.breakpoints.down("sm")]: {
    fontSize: "1rem",
  },
}));

const BlackButton = styled(StyledButton)(({ theme }) => ({
  marginBottom: theme.spacing(12),
  backgroundColor: theme.palette.color[700],
  color: theme.palette.color[0],
  padding: theme.spacing(1, 2),
}));

export default function ErrorPage() {
  const theme = useTheme();
  const navigate = useNavigate();
  return (
    <Box
      sx={{
        height: "100vh",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <StyledTitle>404 Not Found</StyledTitle>
      <StyledSubtitle>It looks like you're in uncharted water</StyledSubtitle>

      <BlackButton
        onClick={() => navigate("/")}
        component={motion.button}
        whileHover={{
          backgroundColor: theme.palette.color[700],
          scale: 1.1,
        }}
        whileTap={{
          scale: 0.9,
        }}
        transition={{
          type: "spring",
          damping: 20,
          stiffness: 300,
        }}
      >
        Go back
      </BlackButton>
    </Box>
  );
}
