import {
  CssBaseline,
  responsiveFontSizes,
  createTheme,
  ThemeProvider,
} from "@mui/material";

let theme = createTheme({
  typography: {
    fontFamily: ["sans-serif", "Poppins"],

    weight: {
      thin: 100,
      extraLight: 200,
      light: 300,
      regular: 400,
      medium: 500,
      semiBold: 600,
      bold: 700,
      extraBold: 800,
    },
  },

  palette: {
    color: {
      0: "#ffffff",
      100: "#fcfbfa",
      200: "#dcdada",
      300: "#bdbbba",
      400: "#9e9b9a",
      500: "#817d7c",
      600: "#646261",
      700: "#4a4847",
    },
  },

  radius: {
    xs: "1rem",
    sm: "1.5rem",
    md: "2rem",
    lg: "2.5rem",
    xl: "3rem",
  },

  components: {
    MuiCssBaseline: {
      styleOverrides: `
        body: {
          position: "relative",
          
        },  

        a: {
          cursor: "pointer",
        },

      `,
    },
  },
});

theme = responsiveFontSizes(theme);

export default function BaseStyle({ children }) {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
}
