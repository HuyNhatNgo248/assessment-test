import { createContext, useContext, useState } from "react";

import useFetch from "../hook/useFetch";

const QuerySearchContext = createContext({});

export function QuerySearchProvider({ children }) {
  const [queryInput, setQueryInput] = useState("");
  const [openSnackBar, setOpenSnackBar] = useState(false);

  const { data, dataStatus } = useFetch(
    {
      method: "GET",
      url: `http://localhost:8080/api/v1/corpus/search?word=${queryInput}`,
    },
    queryInput,
    setOpenSnackBar
  );

  return (
    <QuerySearchContext.Provider
      value={{
        data,
        dataStatus,
        queryInput,
        openSnackBar,
        setOpenSnackBar,
        setQueryInput,
      }}
    >
      {children}
    </QuerySearchContext.Provider>
  );
}

export default function QuerySearch() {
  return useContext(QuerySearchContext);
}
