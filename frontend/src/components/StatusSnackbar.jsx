import Snackbar from "@mui/material/Snackbar";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import QuerySearch from "../context/QuerySearchContext";

export default function StatusSnackbar() {
  const { openSnackBar, setOpenSnackBar, dataStatus } = QuerySearch();

  const handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenSnackBar(false);
  };

  const action = (
    <>
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleClose}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </>
  );

  return (
    <Snackbar
      open={openSnackBar}
      autoHideDuration={3000}
      onClose={handleClose}
      message={dataStatus.message}
      action={action}
      severity={dataStatus.status}
    />
  );
}
