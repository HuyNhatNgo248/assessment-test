import LogoCharacter from "../custom/StyledLogoCharacter";
import { Stack, useTheme } from "@mui/material";
import { motion } from "framer-motion";

function AnimatedLogo({ color, letter, animation }) {
  return (
    <motion.div
      {...animation}
      whileTap={{
        scale: 0.9,
      }}
      transition={{
        type: "spring",
        stiffness: 400,
        damping: 20,
      }}
    >
      <LogoCharacter color={color}>{letter}</LogoCharacter>
    </motion.div>
  );
}

export default function Logo(props) {
  const theme = useTheme();
  return (
    <Stack
      direction="row"
      justifyContent="center"
      sx={{
        gap: theme.spacing(1),
        ...props.sx,
      }}
    >
      <AnimatedLogo
        color="#4285F4"
        letter="C"
        animation={{
          whileHover: {
            y: -10,
            rotate: -20,
            scale: 1.1,
          },
        }}
      />
      <AnimatedLogo
        color="#DB4437"
        letter="o"
        animation={{
          whileHover: {
            y: 10,
            scale: 1.1,
          },
        }}
      />
      <AnimatedLogo
        color="#F4B400"
        letter="r"
        animation={{
          whileHover: {
            y: -10,
            scale: 1.2,
          },
        }}
      />
      <AnimatedLogo
        color="#4285F4"
        letter="p"
        animation={{
          whileHover: {
            y: 10,
            rotate: 20,
            scale: 1.1,
          },
        }}
      />
      <AnimatedLogo
        color="#0F9D58"
        letter="u"
        animation={{
          whileHover: {
            y: -10,
            scale: 1.1,
            rotate: 20,
          },
        }}
      />
      <AnimatedLogo
        color="#DB4437"
        letter="s"
        animation={{
          whileHover: {
            y: 10,
            scale: 1.1,
            rotate: 140,
          },
        }}
      />
    </Stack>
  );
}
