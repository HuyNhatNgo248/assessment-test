import StyledTextField from "../custom/StyledTextField";
import { useTheme } from "@mui/material";

import QuerySearch from "../context/QuerySearchContext";

export default function SearchBar() {
  const { setQueryInput } = QuerySearch();
  const theme = useTheme();

  const inputHandler = (event) => {
    setQueryInput(event.target.value);
  };

  return (
    <StyledTextField
      id="outlined-basic"
      label="Corpus search"
      variant="outlined"
      autoComplete="off"
      sx={{
        width: theme.spacing(65),

        [theme.breakpoints.down("lg")]: {
          width: theme.spacing(60),
        },

        [theme.breakpoints.down("md")]: {
          width: theme.spacing(55),
        },

        [theme.breakpoints.down("sm")]: {
          width: "90%",
        },
      }}
      onChange={inputHandler}
    />
  );
}
