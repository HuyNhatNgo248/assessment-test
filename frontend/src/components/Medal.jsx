import WorkspacePremiumRoundedIcon from "@mui/icons-material/WorkspacePremiumRounded";

const colorMap = {
  0: "#D4AF3f",
  1: "#C0C0C0",
  2: "#b87333",
};

export default function Medal({ type }) {
  return (
    <WorkspacePremiumRoundedIcon
      sx={{
        color: colorMap[type],
      }}
    />
  );
}
