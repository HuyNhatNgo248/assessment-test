import BaseStyle from "./base/BaseStyle";
import SearchPage from "./pages/SearchPage";
import ErrorPage from "./pages/ErrorPage";
import { QuerySearchProvider } from "./context/QuerySearchContext";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

export default function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      errorElement: <ErrorPage />,
      children: [
        {
          path: "/",
          element: (
            <QuerySearchProvider>
              <SearchPage />
            </QuerySearchProvider>
          ),
        },
      ],
    },
  ]);

  return (
    <BaseStyle>
      <RouterProvider router={router} />
    </BaseStyle>
  );
}
