import { styled, Typography } from "@mui/material";

const StyledLogoCharacter = styled(Typography)(({ theme, color }) => ({
  fontSize: "6rem",
  fontWeight: theme.typography.weight["semiBold"],
  color: color || "inherit",
  userSelect: "none",

  [theme.breakpoints.down("lg")]: {
    fontSize: "5rem",
  },

  [theme.breakpoints.down("md")]: {
    fontSize: "4rem",
  },

  [theme.breakpoints.down("sm")]: {
    fontSize: "3.2rem",
  },
}));

export default StyledLogoCharacter;
