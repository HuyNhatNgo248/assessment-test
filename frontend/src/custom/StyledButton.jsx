import { styled, Button } from "@mui/material";

const StyledButton = styled(Button)(({ theme }) => ({
  textTransform: "capitalize",
  fontWeight: theme.typography.weight["semiBold"],
  borderRadius: theme.radius["xs"],

  "&:hover": {
    backgroundColor: "transparent",
  },
}));

export default StyledButton;
