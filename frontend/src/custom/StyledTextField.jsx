import { styled, TextField } from "@mui/material";

const StyledTextField = styled(TextField)(({ theme }) => ({
  ".Mui-focused .MuiOutlinedInput-notchedOutline": {
    border: `solid 1px ${theme.palette.color[200]} !important`,
  },

  ".MuiOutlinedInput-notchedOutline": {
    border: `solid 1px ${theme.palette.color[200]} !important`,
    borderRadius: theme.radius["md"],
  },

  ".MuiInputLabel-root": {
    color: `${theme.palette.color[300]} !important`,
  },

  ".MuiInputBase-input": {
    color: `${theme.palette.color[700]}`,
  },

  "&:hover": {
    boxShadow: "0 1rem 2rem rgba(#000, 0.15)",
  },
}));

export default StyledTextField;
