const express = require("express");
const cors = require("cors");
const compression = require("compression");
const hpp = require("hpp");

const helmet = require("helmet");
const xss = require("xss-clean");
const corpusRouter = require("./routers/corpusRoute");
const AppError = require("./utils/AppError");
const globalErrorHandler = require("./controllers/errorController");

const app = express();

app.use(compression());
app.use(xss());
app.use(cors());
app.use(hpp());
app.use(helmet());

app.use("/api/v1/corpus", corpusRouter);

app.use("*", (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on the server!`, 404));
});

app.use(globalErrorHandler);

module.exports = app;
