const sendErrorDev = (err, req, res) => {
  if (!req.originalUrl.startsWith("/api")) return;

  console.log("❌ Error from Development", err);
  res.status(err.statusCode).json({
    status: err.status,
    message: err.message,
    stack: err.stack,
    error: err,
  });
};

const sendErrorProd = (err, req, res) => {
  if (!req.originalUrl.startsWith("/api")) return;

  if (err.isOperational) {
    console.log("❌ Error from Production ", err);
    return res.status(err.statusCode).json({
      status: err.status,
      message: err.message,
    });
  }

  console.log("❌ Error from Production", err);
  return res.status(500).json({
    status: 500,
    msg: "Please try again later.",
  });
};

module.exports = (err, req, res, next) => {
  err.statusCode = err.statusCode || 500;
  err.status = err.status || "error";

  if (process.env.NODE_ENV === "development") sendErrorDev(err, req, res);
  else if (process.env.NODE_ENV === "production") sendErrorProd(err, req, res);
};
