const { catchAsync } = require("../utils/catchAsync");
const { readFilePro } = require("../utils/readFilePro");
const normalizeWhitespace = require("normalize-html-whitespace");
const TrieCorpus = require("../utils/TrieCorpus");
const HashMapCorpus = require("../utils/HashMapCorpus");

let trieRoot;
let hashRoot;
let set;
let deletedWords = new Set();

exports.getSimilarWords = catchAsync(async (req, res, next) => {
  const word = req.query.word;

  if (!trieRoot) {
    const str = await readFilePro("hemingway.txt");
    trieRoot = new TrieCorpus();
    hashRoot = new HashMapCorpus();

    const strList = normalizeWhitespace(str)
      .trim()
      .replace(/[^a-z0-9' -]/gi, "")
      .split(" ");

    set = new Set(strList);

    let i = 0;
    set.forEach((el) => {
      trieRoot.insert(el);
      hashRoot.insert(el, i++);
    });
  }

  //delete the most similar words
  let similarWords = new Set(
    trieRoot.search(word).sort().slice(0, process.env.MAX_WORDS)
  );

  deletedWords.forEach((el) => similarWords.delete(el));

  if (similarWords.size < process.env.MAX_WORDS) {
    //transform the original words set into list
    const wordList = [...set];

    let len = word.length;
    for (let i = 0; i < len; i++) {
      //retrieve the list of index based on similar character
      const candidates = hashRoot.search(word.at(i).toLowerCase());
      if (!candidates.size) continue;

      //delete the most similar words
      candidates.forEach(
        (el) => deletedWords.has(wordList[el]) && candidates.delete(el)
      );

      //add newly filtered words to similarWords set
      [...candidates].forEach((idx) => similarWords.add(wordList[idx]));

      if (similarWords.size >= process.env.MAX_WORDS) break;
    }
  }

  const results = [...similarWords].sort().slice(0, process.env.MAX_WORDS);

  //keep track of the most similar word for subsequent searches removal
  deletedWords.add(results[0]);

  res.status(200).json({
    status: "success",
    similarWords: results,
  });
});
