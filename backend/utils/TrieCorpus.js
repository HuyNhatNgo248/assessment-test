class TrieCorpus {
  completeString = false;
  children = {};
  wordArr = []; //duplicate words

  insert(word) {
    let node = this;
    word
      .toLowerCase()
      .split("")
      .forEach((ch) => {
        if (!node.children[ch]) node.children[ch] = new TrieCorpus();
        node = node.children[ch];
      });

    node.completeString = true;
    node.wordArr.push(word);
  }

  search(word) {
    let node = this;

    const arr = word.toLowerCase().split("");
    let result = [];
    let tmp = [];

    for (const ch of arr) {
      if (!node.children[ch]) break;
      retrieveString(node.children[ch].children || {});
      if (tmp.length) result = tmp;
      tmp = [];
      node = node.children[ch];
    }

    return result;

    function retrieveString(children) {
      for (const key in children) {
        const obj = children[key];
        if (obj.completeString) tmp.push(...obj.wordArr);
        retrieveString(obj.children);
      }
    }
  }
}

module.exports = TrieCorpus;
