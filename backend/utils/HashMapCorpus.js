class HashMapCorpus {
  map;

  constructor() {
    this.map = {};
  }

  //create a map where key = charater, value = [indexes] where set[index] = element
  insert(word, idx) {
    word
      .toLowerCase()
      .split("")
      .forEach((el) => {
        if (el in this.map) {
          this.map[el].add(idx);
        } else {
          this.map[el] = new Set([idx]);
        }
      });
  }

  search(char) {
    return this.map[char] || new Set([0]);
  }
}

module.exports = HashMapCorpus;
