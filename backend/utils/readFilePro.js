const fs = require("fs");

exports.readFilePro = (fileName) => {
  return new Promise((resolve, reject) => {
    fs.readFile(`${__dirname}/../asset/${fileName}`, "utf-8", (err, data) => {
      err ? reject(`Could not locate file directory 🤨`) : resolve(data);
    });
  });
};
