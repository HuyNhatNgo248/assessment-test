const express = require("express");
const { getSimilarWords } = require("../controllers/corpusController");

const router = express.Router();

router.get("/search", getSimilarWords);

module.exports = router;
