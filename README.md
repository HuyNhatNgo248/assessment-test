## Installation and Run

1. Clone the repository to your local machine.
2. Navigate to the respective `frontend` and `backend` folders and run _npm start_ command on EACH folder.

Note: the frontend React script shall be run on port `3000` whereas the backend Nodejs server will run on port `8080`. Please make sure to keep those two ports _free_ (unused) before executing. If the port is occupied and used by other programs, please perform necessary action to free those two ports.

## Technology Stack

Frontend:

- ReactJS
- React Router
- Material UI
- Framer Motion

Backend:

- NodeJS
- ExpressJS

## Project Description

### Metric of similarity\_

Similarity is dictated on these metrics:

- The relative position of letters within a word
- Letters within a word

### User Interface\_

The design is based on Google's infamous landing page for a clean and user-friendly interface with some added animations on letters for a playful design. Reacts are used as a primary framework on the client-side along with framer-motion for interactivity and material UI's prebuilt components for responsive design and faster development.

1. When user types in the search bar, every keystroke shall generate an event which will NOT immediately trigger an HTTP request to the server. Instead, as soon as user pauses for approximately 300ms, an HTTP request will be sent. The server should response with a JSON object containing `3 most similar words`.

2. Most input with alphabetical characters a-z and numbers 0-9 should mostly give back at least a non-empty response depending on whether a word has been removed from the corpus.

### REST API\_

1. The text file is read and each word is processed to remove special characters. A set is used to remove duplicate words and store a uniqe collection of words.

2. The underlying algorithm is based on Trie (k-ary search tree), Map and Set data structures in order to facility efficient storing and looking up keys.

3. Trie provides the initial list of similar words based on pattern matching with regards to alphabetical similarity and its relative position. However, the matching for a specific pattern may not always return back 3 results to satify the requirements.

4. To address this setback, a Map structure based on JS object literal was introduced. The structure contains "bucket" of individual character which holds a reference to an array of indexes. The index is the position of a word in a set (described in `1`). This is done to reduce memory needed to store a duplicate (if not more) of the data in set. The algorithm will automatically draw from appropriate buckets within the map to satify 3 similar words requirement.

# Full Stack Web Developer Challenge

## Task Description

Your task for this challenge is to create a small search engine comprising of two parts, a web-based user interface and a server component that exposes a REST API which provides search results retrieved from a corpus of text that will be provided to you in `corpus/hemingway.txt`.

Your submission will be evaluated for conforming to the specifications outlined below as well as code quality (maintainability, scalability, performance etc.). You are permitted to use any resources and libraries you wish, however, you should be able to justify design choices in your code.

## Requirements

The basic search engine should be capable of the following three operations.

1. Given a query consisting of a single word `w`, display the 3 most similar words in the search corpus according to some similarity metric of your choosing. You should return results even if `w` is not in the corpus.
2. Given a single word `x`, update the search corpus with `x`. The new word `x` should immediately be
   queryable.
3. Given a single word `y`, remove **_the most similar word_** to `y` in the corpus from further search results.

### User Interface

The user interface should be a browser-based application developed using your JavaScript web framework of choice. It should support the three aforementioned operations. How this is done is completely up to you. Use your creativity and imagination to create a UI that will set your submission apart!

### REST API

The REST API can be implemented using whatever language and frameworks of your choosing. Again, like the UI, it needs to support the three operations listed above. How you choose to accomplish this task is up to you.

## Deliverables

To submit your challenge, fork this repository and provide the link to your forked repository.
You should also update this README to include instructions on how to run your search engine.
Tests are not mandatory but will be considered bonus points if you provide them.

This challenge should take a day at most. It is not expected to be a production ready application and thus will not be evaluated in such a context.
